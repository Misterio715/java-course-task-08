package org.fmavlyutov.repository;

import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;
import org.fmavlyutov.model.Command;

public class CommandRepository {

    public static final Command HELP = new Command(CommandLineConstant.HELP, CommandLineArgument.HELP, "display info about commands and arguments");

    public static final Command VERSION = new Command(CommandLineConstant.VERSION, CommandLineArgument.VERSION, "display program version");

    public static final Command ABOUT = new Command(CommandLineConstant.ABOUT, CommandLineArgument.ABOUT, "display info about author");

    public static final Command INFO = new Command(CommandLineConstant.INFO, CommandLineArgument.INFO, "display system info");

    public static final Command COMMANDS = new Command(CommandLineConstant.COMMANDS, CommandLineArgument.COMMANDS, "display all commands");

    public static final Command ARGUMENTS = new Command(CommandLineConstant.ARGUMENTS, CommandLineArgument.ARGUMENTS, "display all arguments");

    public static final Command EXIT = new Command(CommandLineConstant.EXIT, null, "finish program");

    public static final Command[] TERMINAL_COMMANDS = new Command[] {HELP, VERSION, ABOUT, INFO, EXIT, COMMANDS, ARGUMENTS};

    public static Command[] getCommands() {
        return TERMINAL_COMMANDS;
    }

}
